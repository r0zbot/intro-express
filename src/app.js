import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';

import { setupDB } from './db';

export const app = express();

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.disable('x-powered-by');

export async function bootstrap() {
  const db = await setupDB();

  const handler = (req, res, next) => {
    res.json({ message: 'hello world' });
  };

  app.get('/', handler);

  const projectsRouter = express.Router();

  app.use('/projects', projectsRouter);
  
  projectsRouter.post('/', async (req, res) => {
    const { name } = req.body;

    const slug = name.toLowerCase();

    const project = {
      slug,
      boards: [],
    };

    const existingProject = await db.collection('projects').findOne({ slug });

    if (existingProject) {
      res.status(400).json({ error: `project ${slug} already exists` });
    } else {
      await db.collection('projects').insertOne(project);
      res.json({ project });
    }
  });

  const findProject = async (req, res, next) => {
    const { slug } = req.params;

    const project = await db.collection('projects').findOne({ slug });

    if (!project) {
      res.status(400).json({ error: `project ${slug} does not exist` });
    } else {
      req.body.project = project;
      next();
    }
  };

  projectsRouter.get('/:slug', findProject, async (req, res) => {
    const { project } = req.body;
    res.json({ project });
  });

  projectsRouter.delete('/:slug', findProject, async (req, res) => {
    const { project } = req.body;
    const { slug } = req.params;
    
    const result = await db.collection('projects').deleteOne({ slug });
    
    res.json(result.result);
  });

  projectsRouter.post('/:slug/boards', findProject, async (req, res) => {
    const { name, project } = req.body;
    const board = { name, tasks: [] };
    project.boards.push(board);

    await db
      .collection('projects')
      .findOneAndReplace({ slug: project.slug }, project);

    res.json({ board });
  });

  const findBoard = async (req, res, next) => {
    const { project } = req.body;
    const { name } = req.params;

    const board = project.boards.find(board => board.name == name)
    
    if (!board) {
      res.status(400).json({ error: `board ${name} does not exist` });
    } else {
      req.body.board = board;
      next();
    }
  };

  projectsRouter.delete('/:slug/boards/:name', findProject, findBoard, async (req, res) => {
    const { project } = req.body;
    const { name } = req.params;

    const result = await db
      .collection('projects')
      .updateOne({ slug: project.slug }, {$pull: {'boards': {name}}});

    res.json(result.result);
  });

  projectsRouter.post('/:slug/boards/:name/tasks', findProject, findBoard, async (req, res) => {
    const { name, board, project} = req.body;
    const next_task_id = board.tasks.length == 0 ? 0 : Math.max(...board.tasks.map((a) => a.id)) + 1;
    const task = { name, id: next_task_id };
    board.tasks.push(task);

    await db
      .collection('projects')
      .findOneAndReplace({ slug: project.slug }, project);

    res.json({ board });
  });

  projectsRouter.delete('/:slug/boards/:name/tasks/:id', findProject, findBoard, async (req, res) => {
    const { board, project} = req.body;
    const { id } = req.params;
    const task = board.tasks.find((task) => task.id == id);
    if(!task){
      res.status(400).json({ error: `task with id ${id} does not exist` });
    }
    else{
      board.tasks.splice(board.tasks.indexOf(task), 1);
      await db
        .collection('projects')
        .findOneAndReplace({ slug: project.slug }, project);

      res.json({ board });
    }
    
  });

  return db;
}
