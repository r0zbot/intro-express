import request from 'supertest';

import { app, bootstrap } from '../src/app';

describe('our app', () => {
  let db;

  beforeAll(async () => (db = await bootstrap()));

  beforeEach(async () => {
    await db.dropDatabase(process.env.DB_NAME + '_test');
  });

  afterAll(async () => await db.close());

  // código pt. 1
  test('GET / returns a "hello world"', async () => {
    const { body } = await request(app).get('/');

    expect(body.message).toBeDefined();
    expect(body.message).toBe('hello world');
  });

  // código pt. 2
  test('POST /projects returns a new project slug', async () => {
    const { body } = await request(app)
      .post('/projects')
      .send({ name: 'Awesome' });

    expect(body.project).toBeDefined();
    expect(body.project.slug).toBe('awesome');
  });

  // código pt.2
  test('GET /projects/:slug returns a matching project', async () => {
    await request(app).post('/projects').send({ name: 'Awesome' });

    const { body } = await request(app).get('/projects/awesome');

    expect(body.project).toBeDefined();
    expect(body.project.slug).toBe('awesome');
  });

  // código pt. 3
  test('POST /projects/:slug/boards adds a new board into a project', async () => {
    const project = { name: 'Awesome' };
    const {
      body: {
        project: { slug },
      },
    } = await request(app).post('/projects').send(project);

    const name = 'todo';

    const { body } = await request(app)
      .post(`/projects/${slug}/boards`)
      .send({ name });

    expect(body.board).toBeDefined();
    expect(body.board.name).toBe(name);
  });

  test('DELETE /projects/:slug deletes a project with that slug', async () => {
    const project = { name: 'Awesome' };
    const slug = 'awesome'
    await request(app).post('/projects').send(project);

    await request(app).delete(`/projects/${slug}`);

    const { body } = await request(app).get(`/projects/${slug}`);

    expect(body.error).toBe(`project ${slug} does not exist`);
  });

  test('DELETE /projects/:slug/boards/:name deletes a board in a project with that slug', async () => {
    const project = { name: 'Awesome' };
    const slug = 'awesome';
    const name = 'todo';

    await request(app).post('/projects').send(project);

    await request(app).post(`/projects/${slug}/boards`).send({ name });
    await request(app).delete(`/projects/${slug}/boards/${name}`);

    const { body } = await request(app).get(`/projects/${slug}`);

    expect(body.project).toBeDefined();
    expect(body.project.boards[0]).toBeUndefined();
  });

  test('POST /projects/:slug/boards/:name/tasks adds a task to a board with that name in that slug', async () => {
    const project = { name: 'Awesome' };
    const slug = 'awesome';
    const name = 'todo';
    const task = 'do stuff';


    await request(app).post('/projects').send(project);

    await request(app).post(`/projects/${slug}/boards`).send({ name });

    await request(app).post(`/projects/${slug}/boards/${name}/tasks`).send({ 'name': task });

    const { body } = await request(app).get(`/projects/${slug}`);
    expect(body.project.boards).toBeDefined();
    expect(body.project.boards[0]).toBeDefined();
    expect(body.project.boards[0].tasks[0]).toBeDefined();
    expect(body.project.boards[0].tasks[0].name).toBe(task);
    expect(body.project.boards[0].tasks[0].id).toBeDefined();
  });
  
  test('DELETE /projects/:slug/boards/:name/tasks/:id removes a task to a board with that name in that slug with that id', async () => {
    const project = { name: 'Awesome' };
    const slug = 'awesome';
    const name = 'todo';
    const task = 'do stuff';

    await request(app).post('/projects').send(project);

    await request(app).post(`/projects/${slug}/boards`).send({ name });
    const {'body': body_res} = await request(app).post(`/projects/${slug}/boards/${name}/tasks`).send({ name: task })
    console.log(body_res);
    expect(body_res.board.tasks[0].id).toBeDefined();
    await request(app).delete(`/projects/${slug}/boards/${name}/tasks/${body_res.board.tasks[0].id}`);


    const { body } = await request(app).get(`/projects/${slug}`);
    expect(body.project.boards).toBeDefined();
    expect(body.project.boards[0]).toBeDefined();
    expect(body.project.boards[0].tasks[0]).toBeUndefined();
  });
});
